<?php

use Illuminate\Database\Seeder;

class PoliticaDePrivacidadeSeeder extends Seeder
{
    public function run()
    {
        DB::table('politica_de_privacidade')->insert([
            'texto_pt' => '',
            'texto_en' => '',
        ]);
    }
}
