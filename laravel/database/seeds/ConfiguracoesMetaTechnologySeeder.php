<?php

use Illuminate\Database\Seeder;

class ConfiguracoesMetaTechnologySeeder extends Seeder
{
    public function run()
    {
        DB::table('configuracoes_meta_technology')->insert([
            'nome_do_site' => '',
            'title' => '',
            'description' => '',
            'keywords' => '',
            'imagem_de_compartilhamento' => '',
        ]);
    }
}
