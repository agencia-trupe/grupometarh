<?php

use Illuminate\Database\Seeder;

class MetaTechnologySeeder extends Seeder
{
    public function run()
    {
        DB::table('meta_technology')->insert([
            'abertura_pt' => '',
            'abertura_en' => '',
            'frase_pt' => '',
            'frase_en' => '',
            'video_pt' => '',
            'video_en' => '',
            'chamada_1_titulo_pt' => '',
            'chamada_1_titulo_en' => '',
            'chamada_1_texto_pt' => '',
            'chamada_1_texto_en' => '',
            'chamada_2_titulo_pt' => '',
            'chamada_2_titulo_en' => '',
            'chamada_2_texto_pt' => '',
            'chamada_2_texto_en' => '',
            'chamada_3_titulo_pt' => '',
            'chamada_3_titulo_en' => '',
            'chamada_3_texto_pt' => '',
            'chamada_3_texto_en' => '',
        ]);
    }
}
