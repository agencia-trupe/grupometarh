<?php

use Illuminate\Database\Seeder;

class TextosSeeder extends Seeder
{
    public function run()
    {
        DB::table('textos')->insert([
            'frase_pt' => '',
            'frase_en' => '',
            'missao_pt' => '',
            'missao_en' => '',
            'visao_pt' => '',
            'visao_en' => '',
            'valores_pt' => '',
            'valores_en' => '',
            'diferenciais_competitivos_pt' => '',
            'diferenciais_competitivos_en' => '',
            'responsabilidade_social_pt' => '',
            'responsabilidade_social_en' => '',
        ]);
    }
}
