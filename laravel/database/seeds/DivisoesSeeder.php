<?php

use Illuminate\Database\Seeder;

class DivisoesSeeder extends Seeder
{
    public function run()
    {
        DB::table('divisoes')->insert([
            'meta_bpo_site' => '',
            'meta_bpo_vagas' => '',
            'meta_executivos_site' => '',
            'meta_executivos_vagas' => '',
            'meta_talentos_site' => '',
            'meta_talentos_vagas' => '',
            'meta_technology_site' => '',
            'meta_technology_vagas' => '',
        ]);
    }
}
