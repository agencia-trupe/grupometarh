<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivisoesTable extends Migration
{
    public function up()
    {
        Schema::create('divisoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('meta_bpo_site');
            $table->string('meta_bpo_vagas');
            $table->string('meta_executivos_site');
            $table->string('meta_executivos_vagas');
            $table->string('meta_talentos_site');
            $table->string('meta_talentos_vagas');
            $table->string('meta_technology_site');
            $table->string('meta_technology_vagas');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('divisoes');
    }
}
