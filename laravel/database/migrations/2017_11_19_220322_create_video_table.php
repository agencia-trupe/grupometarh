<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoTable extends Migration
{
    public function up()
    {
        Schema::create('video', function (Blueprint $table) {
            $table->increments('id');
            $table->string('video_pt');
            $table->string('video_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('video');
    }
}
