<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguracoesMetaTechnologyTable extends Migration
{
    public function up()
    {
        Schema::create('configuracoes_meta_technology', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_do_site');
            $table->string('title');
            $table->text('description');
            $table->text('keywords');
            $table->string('imagem_de_compartilhamento');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('configuracoes_meta_technology');
    }
}
