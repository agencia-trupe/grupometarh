<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatosRecebidosEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contatos_recebidos_empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('empresa');
            $table->string('telefone');
            $table->string('email');
            $table->string('contratacao');
            $table->string('regiao');
            $table->text('como_conheceu');
            $table->text('descricao');
            $table->string('arquivo');
            $table->boolean('lido')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contatos_recebidos_empresas');
    }
}
