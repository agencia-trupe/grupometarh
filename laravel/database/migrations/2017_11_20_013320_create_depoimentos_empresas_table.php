<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepoimentosEmpresasTable extends Migration
{
    public function up()
    {
        Schema::create('depoimentos_empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('marca');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->string('autor_pt');
            $table->string('autor_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('depoimentos_empresas');
    }
}
