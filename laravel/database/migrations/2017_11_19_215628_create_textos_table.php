<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextosTable extends Migration
{
    public function up()
    {
        Schema::create('textos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('frase_pt');
            $table->text('frase_en');
            $table->text('missao_pt');
            $table->text('missao_en');
            $table->text('visao_pt');
            $table->text('visao_en');
            $table->text('valores_pt');
            $table->text('valores_en');
            $table->text('diferenciais_competitivos_pt');
            $table->text('diferenciais_competitivos_en');
            $table->text('responsabilidade_social_pt');
            $table->text('responsabilidade_social_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('textos');
    }
}
