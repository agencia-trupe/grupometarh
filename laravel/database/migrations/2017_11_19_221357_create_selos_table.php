<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSelosTable extends Migration
{
    public function up()
    {
        Schema::create('selos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('selos');
    }
}
