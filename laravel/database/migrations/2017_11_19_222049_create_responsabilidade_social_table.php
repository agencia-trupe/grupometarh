<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsabilidadeSocialTable extends Migration
{
    public function up()
    {
        Schema::create('responsabilidade_social', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('marca');
            $table->string('nome');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('responsabilidade_social');
    }
}
