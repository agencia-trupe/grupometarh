<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetaTechnologyTable extends Migration
{
    public function up()
    {
        Schema::create('meta_technology', function (Blueprint $table) {
            $table->increments('id');
            $table->text('abertura_pt');
            $table->text('abertura_en');
            $table->text('frase_pt');
            $table->text('frase_en');
            $table->string('video_pt');
            $table->string('video_en');
            $table->string('chamada_1_titulo_pt');
            $table->string('chamada_1_titulo_en');
            $table->text('chamada_1_texto_pt');
            $table->text('chamada_1_texto_en');
            $table->string('chamada_2_titulo_pt');
            $table->string('chamada_2_titulo_en');
            $table->text('chamada_2_texto_pt');
            $table->text('chamada_2_texto_en');
            $table->string('chamada_3_titulo_pt');
            $table->string('chamada_3_titulo_en');
            $table->text('chamada_3_texto_pt');
            $table->text('chamada_3_texto_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('meta_technology');
    }
}
