<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Depoimentoempresa extends Model
{
    protected $table = 'depoimentos_empresas';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_marca()
    {
        return CropImage::make('marca', [
            'width'   => 180,
            'height'  => 180,
            'bgcolor' => '#fff',
            'path'    => 'assets/img/depoimentos-empresas/'
        ]);
    }
}
