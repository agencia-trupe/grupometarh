<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Divisoes extends Model
{
    protected $table = 'divisoes';

    protected $guarded = ['id'];

}
