<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class PoliticaDePrivacidade extends Model
{
    protected $table = 'politica_de_privacidade';

    protected $guarded = ['id'];

}
