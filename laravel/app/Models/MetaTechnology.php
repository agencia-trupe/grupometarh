<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class MetaTechnology extends Model
{
    protected $table = 'meta_technology';

    protected $guarded = ['id'];

}
