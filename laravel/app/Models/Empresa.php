<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Empresa extends Model
{
    protected $table = 'responsabilidade_social';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_marca()
    {
        return CropImage::make('marca', [
            'width'  => null,
            'height' => null,
            'path'   => 'assets/img/responsabilidade-social/'
        ]);
    }
}
