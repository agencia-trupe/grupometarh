<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Textos extends Model
{
    protected $table = 'textos';

    protected $guarded = ['id'];

}
