<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Casemeta extends Model
{
    protected $table = 'cases';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_marca()
    {
        return CropImage::make('marca', [
            'width'  => 100,
            'height' => null,
            'path'   => 'assets/img/cases/'
        ]);
    }
}
