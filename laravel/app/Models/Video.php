<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Video extends Model
{
    protected $table = 'video';

    protected $guarded = ['id'];

}
