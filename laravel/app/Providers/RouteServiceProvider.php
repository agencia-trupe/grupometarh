<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('configuracoes-meta-technology', 'App\Models\ConfiguracoesMetaTechnology');
        $router->model('contatos-recebidos', 'App\Models\ContatoRecebidoTech');
        $router->model('servicos', 'App\Models\Servico');
        $router->model('meta-technology', 'App\Models\MetaTechnology');
        $router->model('depoimentos-empresas', 'App\Models\Depoimentoempresa');
        $router->model('depoimentos-profissionais', 'App\Models\Depoimentoprofissional');
        $router->model('cases', 'App\Models\Casemeta');
        $router->model('politica-de-privacidade', 'App\Models\PoliticaDePrivacidade');
        $router->model('clientes', 'App\Models\Cliente');
        $router->model('responsabilidade-social', 'App\Models\Empresa');
        $router->model('executivos', 'App\Models\Executivo');
        $router->model('selos', 'App\Models\Selo');
        $router->model('video', 'App\Models\Video');
        $router->model('textos', 'App\Models\Textos');
        $router->model('linha-do-tempo', 'App\Models\Data');
        $router->model('divisoes', 'App\Models\Divisoes');
        $router->model('banners', 'App\Models\Banner');
        $router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos-candidatos', 'App\Models\ContatoRecebido');
        $router->model('recebidos-empresas', 'App\Models\ContatoRecebidoEmpresa');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
