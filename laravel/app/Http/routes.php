<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('executivos', 'HomeController@executivos')->name('executivos');
    Route::get('cases/{divisao?}', 'CasesController@index')->name('cases');
    Route::get('depoimentos/{tipo?}', 'DepoimentosController@index')->name('depoimentos');
    Route::get('politica-de-privacidade', 'HomeController@politica')->name('politica');
    Route::get('contato-candidatos', 'HomeController@contatoCandidatos')->name('contato.candidatos');
    Route::post('contato-candidatos', 'HomeController@postCandidatos')->name('post.candidatos');
    Route::get('contato-empresas', 'HomeController@contatoEmpresas')->name('contato.empresas');
    Route::post('contato-empresas', 'HomeController@postEmpresas')->name('post.empresas');

    Route::get('meta-technology', 'MetaTechnologyController@index')->name('meta-technology');
    Route::post('meta-technology/contato', 'MetaTechnologyController@contato')->name('meta-technology.contato');

    // Localização
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if ($idioma == 'pt' || $idioma == 'en') {
            Session::put('locale', $idioma);
        }
        return redirect()->back();
    })->name('lang');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('configuracoes-meta-technology', 'ConfiguracoesMetaTechnologyController', ['only' => ['index', 'update']]);
        Route::get('meta-technology/contatos-recebidos/{contatos_recebidos}/toggle', ['as' => 'painel.meta-technology.contatos-recebidos.toggle', 'uses' => 'ContatosRecebidosTechController@toggle']);
        Route::resource('meta-technology/contatos-recebidos', 'ContatosRecebidosTechController');
        Route::resource('meta-technology/servicos', 'ServicosController');
        Route::resource('meta-technology', 'MetaTechnologyController', ['only' => ['index', 'update']]);
        Route::resource('depoimentos-empresas', 'DepoimentosEmpresasController');
        Route::resource('depoimentos-profissionais', 'DepoimentosProfissionaisController');
        Route::resource('cases', 'CasesController');
        Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
        Route::resource('clientes', 'ClientesController');
        Route::resource('responsabilidade-social', 'ResponsabilidadeSocialController');
        Route::resource('executivos', 'ExecutivosController');
        Route::resource('selos', 'SelosController');
        Route::resource('video', 'VideoController', ['only' => ['index', 'update']]);
        Route::resource('textos', 'TextosController', ['only' => ['index', 'update']]);
        Route::resource('linha-do-tempo', 'LinhaDoTempoController');
        Route::resource('divisoes', 'DivisoesController', ['only' => ['index', 'update']]);
        Route::resource('banners', 'BannersController');
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos-candidatos/{recebidos_candidatos}/toggle', ['as' => 'painel.contato.recebidos-candidatos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos-candidatos', 'ContatosRecebidosController');
        Route::get('contato/recebidos-empresas/{recebidos_empresas}/toggle', ['as' => 'painel.contato.recebidos-empresas.toggle', 'uses' => 'ContatosRecebidosEmpresasController@toggle']);
        Route::resource('contato/recebidos-empresas', 'ContatosRecebidosEmpresasController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
