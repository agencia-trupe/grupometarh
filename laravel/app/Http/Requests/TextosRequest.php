<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TextosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'missao_pt' => 'required',
            'missao_en' => 'required',
            'visao_pt' => 'required',
            'visao_en' => 'required',
            'valores_pt' => 'required',
            'valores_en' => 'required',
            'diferenciais_competitivos_pt' => 'required',
            'diferenciais_competitivos_en' => 'required',
            'responsabilidade_social_pt' => 'required',
            'responsabilidade_social_en' => 'required',
        ];
    }
}
