<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ExecutivosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem' => 'required|image',
            'nome' => 'required',
            'cargo_pt' => 'required',
            'cargo_en' => 'required',
            'texto_pt' => 'required',
            'texto_en' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
