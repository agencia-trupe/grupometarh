<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DepoimentosEmpresasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'marca' => 'required|image',
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'autor_pt' => 'required',
            'autor_en' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['marca'] = 'image';
        }

        return $rules;
    }
}
