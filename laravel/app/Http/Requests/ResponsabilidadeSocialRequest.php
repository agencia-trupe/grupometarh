<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ResponsabilidadeSocialRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'marca' => 'required|image',
            'nome' => 'required',
            'texto_pt' => 'required',
            'texto_en' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['marca'] = 'image';
        }

        return $rules;
    }
}
