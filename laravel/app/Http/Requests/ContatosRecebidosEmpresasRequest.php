<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatosRecebidosEmpresasRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'          => 'required',
            'empresa'       => 'required',
            'email'         => 'required|email',
            'telefone'      => 'required',
            'contratacao'   => 'required',
            'regiao'        => 'required',
            'como_conheceu' => 'required',
        ];
    }

    public function messages() {
        return [
            'required' => trans('frontend.form-erro'),
            'email'    => trans('frontend.form-erro'),
        ];
    }
}
