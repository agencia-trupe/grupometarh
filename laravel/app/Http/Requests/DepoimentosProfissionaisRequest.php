<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DepoimentosProfissionaisRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'autor_pt' => 'required',
            'autor_en' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
