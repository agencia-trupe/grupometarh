<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClientesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'marca' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['marca'] = 'image';
        }

        return $rules;
    }
}
