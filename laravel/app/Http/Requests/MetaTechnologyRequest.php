<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MetaTechnologyRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'abertura_pt' => 'required',
            'abertura_en' => 'required',
            'frase_pt' => 'required',
            'frase_en' => 'required',
            'video_pt' => 'required',
            'video_en' => 'required',
            'chamada_1_titulo_pt' => 'required',
            'chamada_1_titulo_en' => 'required',
            'chamada_1_texto_pt' => 'required',
            'chamada_1_texto_en' => 'required',
            'chamada_2_titulo_pt' => 'required',
            'chamada_2_titulo_en' => 'required',
            'chamada_2_texto_pt' => 'required',
            'chamada_2_texto_en' => 'required',
            'chamada_3_titulo_pt' => 'required',
            'chamada_3_titulo_en' => 'required',
            'chamada_3_texto_pt' => 'required',
            'chamada_3_texto_en' => 'required',
        ];
    }
}
