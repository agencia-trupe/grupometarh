<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DivisoesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'meta_bpo_site' => '',
            'meta_bpo_vagas' => '',
            'meta_executivos_site' => '',
            'meta_executivos_vagas' => '',
            'meta_talentos_site' => '',
            'meta_talentos_vagas' => '',
            'meta_technology_site' => '',
            'meta_technology_vagas' => '',
        ];
    }
}
