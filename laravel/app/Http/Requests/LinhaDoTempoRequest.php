<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LinhaDoTempoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'data_pt' => 'required',
            'data_en' => 'required',
            'texto_pt' => 'required',
            'texto_en' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
