<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosTechRequest;

use App\Models\MetaTechnology;
use App\Models\Servico;
use App\Models\Contato;
use App\Models\ContatoRecebidoTech;

class MetaTechnologyController extends Controller
{
    public function index()
    {
        $textos   = MetaTechnology::first();
        $servicos = Servico::ordenados()->get();
        $contato  = Contato::first();

        $urlJson = 'http://www.metanews.com.br/mais-recentes';
        $str = file_get_contents($urlJson);
        $metanews = json_decode($str, true);

        return view('frontend.meta-technology.home', compact('textos', 'servicos', 'contato', 'metanews'));
    }

    public function contato(ContatosRecebidosTechRequest $request, ContatoRecebidoTech $contatoRecebido)
    {
        $input = $request->all();

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato-tech', $input, function($message) use ($request, $contato)
            {
                $message->to($contato->email, 'Meta Technology')
                        ->subject('[CONTATO] Meta Technology')
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.contato.sucesso')
        ];

        return response()->json($response);
    }
}
