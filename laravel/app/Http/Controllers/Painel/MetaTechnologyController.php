<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MetaTechnologyRequest;
use App\Http\Controllers\Controller;

use App\Models\MetaTechnology;

class MetaTechnologyController extends Controller
{
    public function index()
    {
        $registro = MetaTechnology::first();

        return view('painel.meta-technology.edit', compact('registro'));
    }

    public function update(MetaTechnologyRequest $request, MetaTechnology $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.meta-technology.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
