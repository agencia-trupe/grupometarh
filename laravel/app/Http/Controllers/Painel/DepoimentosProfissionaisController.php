<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DepoimentosProfissionaisRequest;
use App\Http\Controllers\Controller;

use App\Models\Depoimentoprofissional;

class DepoimentosProfissionaisController extends Controller
{
    public function index()
    {
        $registros = Depoimentoprofissional::ordenados()->get();

        return view('painel.depoimentos-profissionais.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.depoimentos-profissionais.create');
    }

    public function store(DepoimentosProfissionaisRequest $request)
    {
        try {

            $input = $request->all();

            Depoimentoprofissional::create($input);

            return redirect()->route('painel.depoimentos-profissionais.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Depoimentoprofissional $registro)
    {
        return view('painel.depoimentos-profissionais.edit', compact('registro'));
    }

    public function update(DepoimentosProfissionaisRequest $request, Depoimentoprofissional $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.depoimentos-profissionais.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Depoimentoprofissional $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.depoimentos-profissionais.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
