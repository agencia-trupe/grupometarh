<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ContatoRecebidoTech;

class ContatosRecebidosTechController extends Controller
{
    public function index()
    {
        $contatosrecebidos = ContatoRecebidoTech::orderBy('created_at', 'DESC')->get();

        return view('painel.contato.recebidos-tech.index', compact('contatosrecebidos'));
    }

    public function show(ContatoRecebidoTech $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.contato.recebidos-tech.show', compact('contato'));
    }

    public function destroy(ContatoRecebidoTech $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.meta-technology.contatos-recebidos.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(ContatoRecebidoTech $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.meta-technology.contatos-recebidos.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
