<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DepoimentosEmpresasRequest;
use App\Http\Controllers\Controller;

use App\Models\Depoimentoempresa;

class DepoimentosEmpresasController extends Controller
{
    public function index()
    {
        $registros = Depoimentoempresa::ordenados()->get();

        return view('painel.depoimentos-empresas.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.depoimentos-empresas.create');
    }

    public function store(DepoimentosEmpresasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['marca'])) $input['marca'] = Depoimentoempresa::upload_marca();

            Depoimentoempresa::create($input);

            return redirect()->route('painel.depoimentos-empresas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Depoimentoempresa $registro)
    {
        return view('painel.depoimentos-empresas.edit', compact('registro'));
    }

    public function update(DepoimentosEmpresasRequest $request, Depoimentoempresa $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['marca'])) $input['marca'] = Depoimentoempresa::upload_marca();

            $registro->update($input);

            return redirect()->route('painel.depoimentos-empresas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Depoimentoempresa $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.depoimentos-empresas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
