<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ExecutivosRequest;
use App\Http\Controllers\Controller;

use App\Models\Executivo;

class ExecutivosController extends Controller
{
    public function index()
    {
        $registros = Executivo::ordenados()->get();

        return view('painel.executivos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.executivos.create');
    }

    public function store(ExecutivosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Executivo::upload_imagem();

            Executivo::create($input);

            return redirect()->route('painel.executivos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Executivo $registro)
    {
        return view('painel.executivos.edit', compact('registro'));
    }

    public function update(ExecutivosRequest $request, Executivo $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Executivo::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.executivos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Executivo $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.executivos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
