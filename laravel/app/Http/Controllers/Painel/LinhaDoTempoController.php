<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\LinhaDoTempoRequest;
use App\Http\Controllers\Controller;

use App\Models\Data;

class LinhaDoTempoController extends Controller
{
    public function index()
    {
        $registros = Data::ordenados()->get();

        return view('painel.linha-do-tempo.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.linha-do-tempo.create');
    }

    public function store(LinhaDoTempoRequest $request)
    {
        try {

            $input = $request->all();

            Data::create($input);

            return redirect()->route('painel.linha-do-tempo.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Data $registro)
    {
        return view('painel.linha-do-tempo.edit', compact('registro'));
    }

    public function update(LinhaDoTempoRequest $request, Data $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.linha-do-tempo.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Data $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.linha-do-tempo.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
