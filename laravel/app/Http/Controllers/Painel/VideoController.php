<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VideoRequest;
use App\Http\Controllers\Controller;

use App\Models\Video;

class VideoController extends Controller
{
    public function index()
    {
        $registro = Video::first();

        return view('painel.video.edit', compact('registro'));
    }

    public function update(VideoRequest $request, Video $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.video.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
