<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ResponsabilidadeSocialRequest;
use App\Http\Controllers\Controller;

use App\Models\Empresa;

class ResponsabilidadeSocialController extends Controller
{
    public function index()
    {
        $registros = Empresa::ordenados()->get();

        return view('painel.responsabilidade-social.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.responsabilidade-social.create');
    }

    public function store(ResponsabilidadeSocialRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['marca'])) $input['marca'] = Empresa::upload_marca();

            Empresa::create($input);

            return redirect()->route('painel.responsabilidade-social.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Empresa $registro)
    {
        return view('painel.responsabilidade-social.edit', compact('registro'));
    }

    public function update(ResponsabilidadeSocialRequest $request, Empresa $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['marca'])) $input['marca'] = Empresa::upload_marca();

            $registro->update($input);

            return redirect()->route('painel.responsabilidade-social.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Empresa $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.responsabilidade-social.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
