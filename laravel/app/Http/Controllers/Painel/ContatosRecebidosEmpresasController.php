<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ContatoRecebidoEmpresa;

class ContatosRecebidosEmpresasController extends Controller
{
    public function index()
    {
        $contatosrecebidos = ContatoRecebidoEmpresa::orderBy('created_at', 'DESC')->get();

        return view('painel.contato.recebidos-empresas.index', compact('contatosrecebidos'));
    }

    public function show(ContatoRecebidoEmpresa $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.contato.recebidos-empresas.show', compact('contato'));
    }

    public function destroy(ContatoRecebidoEmpresa $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.contato.recebidos-empresas.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(ContatoRecebidoEmpresa $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.contato.recebidos-empresas.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
