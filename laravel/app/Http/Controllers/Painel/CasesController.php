<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CasesRequest;
use App\Http\Controllers\Controller;

use App\Models\Casemeta;

class CasesController extends Controller
{
    private $divisoes = [
        'meta-bpo'        => 'Meta BPO',
        'meta-executivos' => 'Meta Executivos',
        'meta-talentos'   => 'Meta Talentos',
        'meta-technology' => 'Meta Technology'
    ];

    public function __construct() {
        view()->share('divisoes', $this->divisoes);
    }

    public function index(Request $request)
    {
        $filtro = $request->get('filtro') ?: 'meta-bpo';

        $registros = Casemeta::where('divisao', $filtro)->ordenados()->get();

        return view('painel.cases.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.cases.create');
    }

    public function store(CasesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['marca'])) $input['marca'] = Casemeta::upload_marca();

            Casemeta::create($input);

            return redirect()->route('painel.cases.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Casemeta $registro)
    {
        return view('painel.cases.edit', compact('registro'));
    }

    public function update(CasesRequest $request, Casemeta $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['marca'])) $input['marca'] = Casemeta::upload_marca();

            $registro->update($input);

            return redirect()->route('painel.cases.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Casemeta $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.cases.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
