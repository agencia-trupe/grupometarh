<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ConfiguracoesMetaTechnologyRequest;
use App\Http\Controllers\Controller;

use App\Models\ConfiguracoesMetaTechnology;

class ConfiguracoesMetaTechnologyController extends Controller
{
    public function index()
    {
        $registro = ConfiguracoesMetaTechnology::first();

        return view('painel.configuracoes-meta-technology.edit', compact('registro'));
    }

    public function update(ConfiguracoesMetaTechnologyRequest $request, ConfiguracoesMetaTechnology $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_de_compartilhamento'])) $input['imagem_de_compartilhamento'] = ConfiguracoesMetaTechnology::upload_imagem_de_compartilhamento();

            $registro->update($input);

            return redirect()->route('painel.configuracoes-meta-technology.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
