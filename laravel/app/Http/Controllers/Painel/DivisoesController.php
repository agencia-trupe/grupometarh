<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DivisoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Divisoes;

class DivisoesController extends Controller
{
    public function index()
    {
        $registro = Divisoes::first();

        return view('painel.divisoes.edit', compact('registro'));
    }

    public function update(DivisoesRequest $request, Divisoes $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.divisoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
