<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Depoimentoempresa;
use App\Models\Depoimentoprofissional;

class DepoimentosController extends Controller
{
    public function index($tipo = 'empresas')
    {
        $tipos = ['empresas', 'profissionais'];

        if (!in_array($tipo, $tipos)) return abort('404');

        if ($tipo == 'empresas') {
            $depoimentos = Depoimentoempresa::ordenados()->get();
        } else {
            $depoimentos = Depoimentoprofissional::ordenados()->get();
        }

        return view('frontend.depoimentos', compact('tipo', 'depoimentos'));
    }
}
