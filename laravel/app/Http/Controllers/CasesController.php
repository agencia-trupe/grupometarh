<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Casemeta;

class CasesController extends Controller
{
    public function index($divisao = 'meta-bpo')
    {
        $divisoes = ['meta-bpo', 'meta-executivos', 'meta-talentos', 'meta-technology'];

        if (!in_array($divisao, $divisoes)) return abort('404');

        $cases = Casemeta::where('divisao', $divisao)->ordenados()->get();

        return view('frontend.cases', compact('divisao', 'cases'));
    }
}
