<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Divisoes;
use App\Models\Data;
use App\Models\Textos;
use App\Models\Video;
use App\Models\Selo;
use App\Models\Executivo;
use App\Models\Empresa;
use App\Models\Cliente;
use App\Models\Contato;
use App\Models\PoliticaDePrivacidade;

use App\Models\ContatoRecebido;
use App\Models\ContatoRecebidoEmpresa;
use App\Http\Requests\ContatosRecebidosRequest;
use App\Http\Requests\ContatosRecebidosEmpresasRequest;

class HomeController extends Controller
{
    public function index()
    {
        $banners      = Banner::ordenados()->get();
        $divisoes     = Divisoes::first();
        $linhaDoTempo = Data::ordenados()->get()->groupBy('data_'.app()->getLocale());
        $textos       = Textos::first();
        $video        = Video::first();
        $selos        = Selo::ordenados()->get();
        $empresas     = Empresa::ordenados()->get();
        $clientes     = Cliente::orderByRaw('RAND()')->get();
        $contato      = Contato::first();

        $urlJson = 'http://www.metanews.com.br/mais-recentes';
        $str = file_get_contents($urlJson);
        $metanews = json_decode($str, true);

        return view('frontend.home', compact('banners', 'divisoes', 'linhaDoTempo', 'textos', 'video', 'selos', 'empresas', 'clientes', 'contato', 'metanews'));
    }

    public function executivos()
    {
        $executivos = Executivo::ordenados()->get();

        return view('frontend.executivos', compact('executivos'));
    }

    public function politica()
    {
        $politica = PoliticaDePrivacidade::first();

        return view('frontend.politica', compact('politica'));
    }

    public function contatoCandidatos()
    {
        $divisoes = Divisoes::first();

        return view('frontend.contato-candidatos', compact('divisoes'));
    }

    public function postCandidatos(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $input = $request->all();

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $input, function($message) use ($request, $contato)
            {
                $message->to($contato->email, 'Grupo Meta RH')
                        ->subject('[CONTATO CANDIDATOS] Grupo Meta RH')
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.contato.sucesso')
        ];

        return response()->json($response);
    }

    public function contatoEmpresas()
    {
        return view('frontend.contato-empresas');
    }

    public function postEmpresas(ContatosRecebidosEmpresasRequest $request, ContatoRecebidoEmpresa $contatoRecebido)
    {
        $input = $request->all();

        if ($request->hasFile('arquivo')) {
            $input['arquivo'] = ContatoRecebidoEmpresa::upload_arquivo();
        } else {
            $input['arquivo'] = '';
        }

        $input['contratacao'] = implode(' / ', $input['contratacao']);

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato-empresas', $input, function($message) use ($request, $contato)
            {
                $message->to($contato->email, 'Grupo Meta RH')
                        ->subject('[CONTATO EMPRESAS] Grupo Meta RH')
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.contato.sucesso')
        ];

        return response()->json($response);
    }
}
