<?php

return [

    'nav' => [
        'grupo'                => 'GRUPO META RH',
        'quem-somos'           => 'About Us',
        'executivos'           => 'Our Executives',
        'empresas-grupo'       => 'Grupo Meta RH’ divisions',
        'missao-visao-valores' => 'Mission, Vision and Values',
        'responsabilidade'     => 'Social Responsibility',
        'clientes'             => 'CLIENTS',
        'cases'                => 'CASES',
        'depoimentos'          => 'TESTIMONIALS',
        'empresas'             => 'Companies',
        'profissionais'        => 'Professionals',
        'contato'              => 'CONTACT',
        'candidatos'           => 'For Candidates',
        'para-empresas'        => 'For Companies',
        'colaboradores'        => 'COLLABORATORS’ SERVICES',
        'area-restrita'        => 'RESTRICTED AREA',
        'politica'             => 'Privacy Policy',
    ],

    'home' => [
        'conheca'          => 'KNOW OUR POSITIONS AND SPECIALIZED DIVISIONS:',
        'acessar-site'     => 'access the site',
        'ver-vagas'        => 'access positions',
        'quem-somos'       => 'ABOUT US',
        'missao'           => 'Mission',
        'visao'            => 'Vision',
        'valores'          => 'Values',
        'diferenciais'     => 'COMPETITIVE DIFERENTIALS',
        'responsabilidade' => 'OUR SOCIAL RESPONSIBILITY',
        'clientes'         => 'OUR CLIENTS',
        'executivos'       => 'OUR EXECUTIVES',
        'executivos-texto' => 'We have professionals with extensive experience in the human resources market.',
        'conheca-mais'     => 'KNOW MORE'
    ],

    'contato' => [
        'titulo-candidatos' => 'Submit your resume',
        'overlay'           => 'SUBMIT YOUR RESUME',
        'duvidas'           => 'Solve your doubts:',
        'nome'              => 'name',
        'telefone'          => 'phone',
        'mensagem'          => 'message',
        'como_conheceu'     => 'how did you find us?',
        'descricao'         => 'job description',
        'enviar'            => 'SEND',
        'sucesso'           => 'Message sent successfully!',
        'contrate'          => 'HIRE WITH US',
        'recrute'           => 'SEND YOUR POSITIONS’ INFORMATIONS AND RECRUIT WITH US:',
        'politica'          => 'Check out our <a href="politica-de-privacidade">Privacy Policy</a> for the information you provide here.',
        'empresa'           => 'Company',
        'contratacao'       => 'Type of hiring:',
        'efetivo'           => 'Permanent worker',
        'temporario'        => 'Temporary',
        'terceiro'          => 'Outsourcing',
        'estagio'           => 'Internship',
        'trainee'           => 'Trainee',
        'regiao'            => 'region: SELECT (all national territory)',
        'norte'             => 'North',
        'nordeste'          => 'Northeast',
        'sudeste'           => 'Southeast',
        'sul'               => 'South',
        'centro-oeste'      => 'Midwest',
        'anexar'            => 'attach file of job description (if desired)',
        'indicacoes'        => 'Indications',
        'redes-sociais'     => 'Social Media',
        'outros'            => 'Other',
    ],

    'form-erro' => 'Complete all the fields correctly',

    'noticias' => 'ALL SECTOR NEWS REUNITED IN A PORTAL',

    'nossos-cases' => 'OUR CASES',

    '404' => 'Page not found',

    'copyright' => [
        'direitos' => 'All rights reserved.',
        'criacao'  => 'Sites creation'
    ],

    'meta-technology' => [
        'voltar'   => 'VISIT THE META RH GROUP WEBSITE &raquo;',
        'servicos' => 'SERVICES'
    ]

];
