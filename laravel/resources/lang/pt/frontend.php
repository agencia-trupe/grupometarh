<?php

return [

    'nav' => [
        'grupo'                => 'GRUPO META RH',
        'quem-somos'           => 'Quem somos',
        'executivos'           => 'Nossos Executivos',
        'empresas-grupo'       => 'Empresas do Grupo',
        'missao-visao-valores' => 'Missão, Visão e Valores',
        'responsabilidade'     => 'Responsabilidade Social',
        'clientes'             => 'CLIENTES',
        'cases'                => 'CASES',
        'depoimentos'          => 'DEPOIMENTOS',
        'empresas'             => 'Empresas',
        'profissionais'        => 'Profissionais',
        'contato'              => 'CONTATO',
        'candidatos'           => 'Para Candidatos',
        'para-empresas'        => 'Para Empresas',
        'colaboradores'        => 'SERVIÇOS PARA COLABORADORES',
        'area-restrita'        => 'ÁREA RESTRITA',
        'politica'             => 'Política de Privacidade',
    ],

    'home' => [
        'conheca'          => 'CONHEÇA NOSSAS VAGAS E DIVISÕES ESPECIALIZADAS:',
        'acessar-site'     => 'acessar o site',
        'ver-vagas'        => 'ver vagas',
        'quem-somos'       => 'QUEM SOMOS',
        'missao'           => 'Missão',
        'visao'            => 'Visão',
        'valores'          => 'Valores',
        'diferenciais'     => 'DIFERENCIAIS COMPETITIVOS',
        'responsabilidade' => 'NOSSA RESPONSABILIDADE SOCIAL',
        'clientes'         => 'NOSSOS CLIENTES',
        'executivos'       => 'NOSSOS EXECUTIVOS',
        'executivos-texto' => 'Contamos com profissionais com ampla experiência no mercado de recursos humanos.',
        'conheca-mais'     => 'CONHEÇA MAIS'
    ],

    'contato' => [
        'titulo-candidatos' => 'Cadastre seu currículo em uma de nossas divisões | Conheça nossas vagas',
        'overlay'           => 'CADASTRE SEU CURRÍCULO<br>CONHEÇA NOSSAS VAGAS',
        'duvidas'           => 'Tire suas dúvidas:',
        'nome'              => 'nome',
        'telefone'          => 'telefone',
        'mensagem'          => 'mensagem',
        'como_conheceu'     => 'como nos conheceu',
        'descricao'         => 'descrição da vaga',
        'enviar'            => 'ENVIAR',
        'sucesso'           => 'Mensagem enviada com sucesso!',
        'contrate'          => 'CONTRATE CONOSCO',
        'recrute'           => 'ENVIE AS INFORMAÇÕES DA SUA VAGA E RECRUTE CONOSCO:',
        'politica'          => 'Confira nossa <a href="politica-de-privacidade">Política de Privacidade</a> para os dados que você disponibiliza aqui.',
        'empresa'           => 'empresa',
        'contratacao'       => 'Tipo de contratação:',
        'efetivo'           => 'Efetivo',
        'temporario'        => 'Temporário',
        'terceiro'          => 'Terceiro',
        'estagio'           => 'Estágio',
        'trainee'           => 'Trainee',
        'regiao'            => 'região: SELECIONE (todo território nacional)',
        'norte'             => 'Norte',
        'nordeste'          => 'Nordeste',
        'sudeste'           => 'Sudeste',
        'sul'               => 'Sul',
        'centro-oeste'      => 'Centro-Oeste',
        'anexar'            => 'anexar arquivo do descritivo da vaga (se desejado)',
        'indicacoes'        => 'Indicações',
        'redes-sociais'     => 'Redes Sociais',
        'outros'            => 'Outros',
    ],

    'form-erro' => 'Preencha todos os campos corretamente',

    'noticias' => 'AS NOTÍCIAS DO SETOR REUNIDAS EM UM SÓ LUGAR',

    'nossos-cases' => 'NOSSOS CASES',

    '404' => 'Página não encontrada',

    'copyright' => [
        'direitos' => 'Todos os direitos reservados.',
        'criacao'  => 'Criação de sites'
    ],

    'meta-technology' => [
        'voltar'   => 'VISITAR O SITE DO GRUPO META RH &raquo;',
        'servicos' => 'SERVIÇOS'
    ]

];
