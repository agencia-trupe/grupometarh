<!DOCTYPE html>
<html>
<head>
    <title>[CONTATO EMPRESAS] {{ $config->nome_do_site }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Empresa:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $empresa }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Tipo de contratação:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $contratacao }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Região:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $regiao }}</span>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Como nos conheceu:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $como_conheceu }}</span>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Descrição da vaga:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $descricao }}</span>
@if(isset($arquivo))
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Arquivo:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'><a href="{{ url('arquivos/'.$arquivo) }}">{{ $arquivo }}</a></span><br>
@endif
</body>
</html>
