@extends('frontend.common.template')

@section('content')

    <div class="not-found">
        <div class="wrapper">
            <h1>{{ trans('frontend.404') }}</h1>
        </div>
    </div>

@endsection
