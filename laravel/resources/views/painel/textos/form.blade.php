@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('missao_pt', 'Missão PT') !!}
            {!! Form::textarea('missao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoUl']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('missao_en', 'Missão EN') !!}
            {!! Form::textarea('missao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoUl']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('visao_pt', 'Visão PT') !!}
            {!! Form::textarea('visao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoUl']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('visao_en', 'Visão EN') !!}
            {!! Form::textarea('visao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoUl']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('valores_pt', 'Valores PT') !!}
            {!! Form::textarea('valores_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoUl']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('valores_en', 'Valores EN') !!}
            {!! Form::textarea('valores_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoUl']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('diferenciais_competitivos_pt', 'Diferenciais Competitivos PT') !!}
            {!! Form::textarea('diferenciais_competitivos_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoUl']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('diferenciais_competitivos_en', 'Diferenciais Competitivos EN') !!}
            {!! Form::textarea('diferenciais_competitivos_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoUl']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('responsabilidade_social_pt', 'Responsabilidade Social PT') !!}
            {!! Form::textarea('responsabilidade_social_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('responsabilidade_social_en', 'Responsabilidade Social EN') !!}
            {!! Form::textarea('responsabilidade_social_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
