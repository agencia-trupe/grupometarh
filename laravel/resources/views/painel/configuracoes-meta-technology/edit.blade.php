@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Meta Technology /</small> Configurações</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.configuracoes-meta-technology.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.configuracoes-meta-technology.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
