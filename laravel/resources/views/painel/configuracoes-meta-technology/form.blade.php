@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome_do_site', 'Nome do site') !!}
    {!! Form::text('nome_do_site', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('title', 'Title') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('description', 'Description') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('keywords', 'Keywords') !!}
    {!! Form::text('keywords', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_de_compartilhamento', 'Imagem de Compartilhamento') !!}
    <img src="{{ url('assets/img/'.$registro->imagem_de_compartilhamento) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem_de_compartilhamento', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
