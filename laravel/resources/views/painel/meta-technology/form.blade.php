@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('abertura_pt', 'Abertura PT') !!}
            {!! Form::textarea('abertura_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('abertura_en', 'Abertura EN') !!}
            {!! Form::textarea('abertura_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('frase_pt', 'Frase PT') !!}
            {!! Form::textarea('frase_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBr']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('frase_en', 'Frase EN') !!}
            {!! Form::textarea('frase_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBr']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('video_pt', 'Vídeo PT') !!}
            {!! Form::text('video_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('video_en', 'Vídeo EN') !!}
            {!! Form::text('video_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('chamada_1_titulo_pt', 'Chamada 1 Título PT') !!}
            {!! Form::text('chamada_1_titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_1_texto_pt', 'Chamada 1 Texto PT') !!}
            {!! Form::textarea('chamada_1_texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('chamada_2_titulo_pt', 'Chamada 2 Título PT') !!}
            {!! Form::text('chamada_2_titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_2_texto_pt', 'Chamada 2 Texto PT') !!}
            {!! Form::textarea('chamada_2_texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('chamada_3_titulo_pt', 'Chamada 3 Título PT') !!}
            {!! Form::text('chamada_3_titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_3_texto_pt', 'Chamada 3 Texto PT') !!}
            {!! Form::textarea('chamada_3_texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBr']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('chamada_1_titulo_en', 'Chamada 1 Título EN') !!}
            {!! Form::text('chamada_1_titulo_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_1_texto_en', 'Chamada 1 Texto EN') !!}
            {!! Form::textarea('chamada_1_texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('chamada_2_titulo_en', 'Chamada 2 Título EN') !!}
            {!! Form::text('chamada_2_titulo_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_2_texto_en', 'Chamada 2 Texto EN') !!}
            {!! Form::textarea('chamada_2_texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('chamada_3_titulo_en', 'Chamada 3 Título EN') !!}
            {!! Form::text('chamada_3_titulo_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_3_texto_en', 'Chamada 3 Texto EN') !!}
            {!! Form::textarea('chamada_3_texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBr']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
