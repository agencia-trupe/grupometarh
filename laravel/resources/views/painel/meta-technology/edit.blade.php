@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Meta Technology</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.meta-technology.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.meta-technology.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
