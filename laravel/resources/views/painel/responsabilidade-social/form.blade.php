@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('marca', 'Marca') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/responsabilidade-social/'.$registro->marca) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('marca', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_pt', 'Texto PT') !!}
            {!! Form::textarea('texto_pt', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto EN') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.responsabilidade-social.index') }}" class="btn btn-default btn-voltar">Voltar</a>
