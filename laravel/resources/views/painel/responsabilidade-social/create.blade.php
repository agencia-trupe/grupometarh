@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Responsabilidade Social /</small> Adicionar Empresa</h2>
    </legend>

    {!! Form::open(['route' => 'painel.responsabilidade-social.store', 'files' => true]) !!}

        @include('painel.responsabilidade-social.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
