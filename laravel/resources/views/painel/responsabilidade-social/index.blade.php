@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Responsabilidade Social
            <a href="{{ route('painel.responsabilidade-social.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Empresa</a>
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="responsabilidade_social">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Marca</th>
                <th>Nome</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td><img src="{{ asset('assets/img/responsabilidade-social/'.$registro->marca) }}" style="max-width: 100%;" alt=""></td>
                <td>{{ $registro->nome }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.responsabilidade-social.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.responsabilidade-social.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
