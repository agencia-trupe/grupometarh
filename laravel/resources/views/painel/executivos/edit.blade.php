@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Executivos /</small> Editar Executivo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.executivos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.executivos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
