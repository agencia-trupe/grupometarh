@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Executivos /</small> Adicionar Executivo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.executivos.store', 'files' => true]) !!}

        @include('painel.executivos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
