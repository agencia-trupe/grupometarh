@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Depoimentos Empresas /</small> Adicionar Depoimento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.depoimentos-empresas.store', 'files' => true]) !!}

        @include('painel.depoimentos-empresas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
