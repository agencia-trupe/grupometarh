@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Depoimentos Empresas /</small> Editar Depoimento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.depoimentos-empresas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.depoimentos-empresas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
