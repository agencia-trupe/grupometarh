@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto_pt', 'Texto PT') !!}
    {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'politica']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_en', 'Texto EN') !!}
    {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'politica']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
