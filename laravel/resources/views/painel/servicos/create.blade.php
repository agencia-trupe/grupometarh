@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Meta Technology / Serviços /</small> Adicionar Serviço</h2>
    </legend>

    {!! Form::open(['route' => 'painel.meta-technology.servicos.store', 'files' => true]) !!}

        @include('painel.servicos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
