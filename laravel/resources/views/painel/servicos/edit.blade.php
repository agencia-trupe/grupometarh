@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Meta Technology / Serviços /</small> Editar Serviço</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.meta-technology.servicos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.servicos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
