@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Depoimentos Profissionais /</small> Editar Depoimento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.depoimentos-profissionais.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.depoimentos-profissionais.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
