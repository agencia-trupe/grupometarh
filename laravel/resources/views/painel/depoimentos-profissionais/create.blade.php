@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Depoimentos Profissionais /</small> Adicionar Depoimento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.depoimentos-profissionais.store', 'files' => true]) !!}

        @include('painel.depoimentos-profissionais.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
