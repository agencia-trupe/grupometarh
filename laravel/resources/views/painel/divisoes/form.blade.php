@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('meta_bpo_site', 'Meta BPO Site') !!}
            {!! Form::text('meta_bpo_site', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('meta_bpo_vagas', 'Meta BPO Vagas') !!}
            {!! Form::text('meta_bpo_vagas', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('meta_executivos_site', 'Meta Executivos Site') !!}
            {!! Form::text('meta_executivos_site', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('meta_executivos_vagas', 'Meta Executivos Vagas') !!}
            {!! Form::text('meta_executivos_vagas', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('meta_talentos_site', 'Meta Talentos Site') !!}
            {!! Form::text('meta_talentos_site', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('meta_talentos_vagas', 'Meta Talentos Vagas') !!}
            {!! Form::text('meta_talentos_vagas', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('meta_technology_site', 'Meta Technology Site') !!}
            {!! Form::text('meta_technology_site', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('meta_technology_vagas', 'Meta Technology Vagas') !!}
            {!! Form::text('meta_technology_vagas', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
