@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Vídeo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.video.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.video.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
