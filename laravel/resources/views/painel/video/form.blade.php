@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('video_pt', 'Vídeo PT') !!}
    {!! Form::text('video_pt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('video_en', 'Vídeo EN') !!}
    {!! Form::text('video_en', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
