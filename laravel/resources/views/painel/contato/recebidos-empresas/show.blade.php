@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Contatos Recebidos (Empresas)</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $contato->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $contato->nome }}</div>
    </div>

    <div class="form-group">
        <label>Empresa</label>
        <div class="well">{{ $contato->empresa }}</div>
    </div>

    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $contato->telefone }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">
            <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $contato->email }}" style="margin-right:5px;border:0;transition:background .3s">
                <span class="glyphicon glyphicon-copy"></span>
            </button>
            {{ $contato->email }}
        </div>
    </div>

    <div class="form-group">
        <label>Tipo de contratação</label>
        <div class="well">{{ $contato->contratacao }}</div>
    </div>

    <div class="form-group">
        <label>Região</label>
        <div class="well">{{ $contato->regiao }}</div>
    </div>

    <div class="form-group">
        <label>Como nos conheceu</label>
        <div class="well">{{ $contato->como_conheceu }}</div>
    </div>

    <div class="form-group">
        <label>Descrição da vaga</label>
        <div class="well">{{ $contato->descricao }}</div>
    </div>

@if($contato->arquivo)
    <div class="form-group">
        <label>Arquivo</label>
        <div class="well">
            <a href="{{ url('arquivos/'.$contato->arquivo) }}" target="_blank">{{ $contato->arquivo }}</a>
        </div>
    </div>
@endif

    <a href="{{ route('painel.contato.recebidos-candidatos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
