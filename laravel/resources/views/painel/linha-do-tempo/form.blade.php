@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('data_pt', 'Data PT (registros com a mesma data serão agrupados)') !!}
            {!! Form::text('data_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_pt', 'Texto PT') !!}
            {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBr']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('data_en', 'Data EN') !!}
            {!! Form::text('data_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto EN') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBr']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.linha-do-tempo.index') }}" class="btn btn-default btn-voltar">Voltar</a>
