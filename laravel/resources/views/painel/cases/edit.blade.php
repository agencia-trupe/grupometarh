@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Cases /</small> Editar Case</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.cases.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cases.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
