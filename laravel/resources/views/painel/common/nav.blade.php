<ul class="nav navbar-nav">
    <li class="dropdown @if(str_is('painel.textos*', Route::currentRouteName()) || str_is('painel.banners*', Route::currentRouteName()) || str_is('painel.divisoes*', Route::currentRouteName()) || str_is('painel.linha-do-tempo*', Route::currentRouteName()) || str_is('painel.video*', Route::currentRouteName()) || str_is('painel.selos*', Route::currentRouteName()) || str_is('painel.executivos*', Route::currentRouteName()) || str_is('painel.responsabilidade-social*', Route::currentRouteName()) || str_is('painel.clientes*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Home
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.textos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.textos.index') }}">Textos</a>
            </li>
        	<li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        		<a href="{{ route('painel.banners.index') }}">Banners</a>
        	</li>
            <li @if(str_is('painel.divisoes*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.divisoes.index') }}">Divisões</a>
            </li>
            <li @if(str_is('painel.linha-do-tempo*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.linha-do-tempo.index') }}">Linha do Tempo</a>
            </li>
            <li @if(str_is('painel.video*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.video.index') }}">Vídeo</a>
            </li>
            <li @if(str_is('painel.selos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.selos.index') }}">Selos</a>
            </li>
            <li @if(str_is('painel.executivos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.executivos.index') }}">Executivos</a>
            </li>
            <li @if(str_is('painel.responsabilidade-social*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.responsabilidade-social.index') }}">Responsabilidade Social</a>
            </li>
            <li @if(str_is('painel.clientes*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.clientes.index') }}">Clientes</a>
            </li>
        </ul>
    </li>
    <li @if(str_is('painel.cases*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.cases.index') }}">Cases</a>
    </li>
    <li class="dropdown @if(str_is('painel.depoimentos*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Depoimentos
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.depoimentos-empresas*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.depoimentos-empresas.index') }}">Empresas</a>
            </li>
            <li @if(str_is('painel.depoimentos-profissionais*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.depoimentos-profissionais.index') }}">Profissionais</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos + $contatosNaoLidosEmpresas >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos + $contatosNaoLidosEmpresas }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos-candidatos.index') }}">
                Contatos Recebidos (Candidatos)
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
            <li><a href="{{ route('painel.contato.recebidos-empresas.index') }}">
                Contatos Recebidos (Empresas)
                @if($contatosNaoLidosEmpresas >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidosEmpresas }}</span>
                @endif
            </a></li>
        </ul>
    </li>
    <li @if(str_is('painel.politica-de-privacidade*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.politica-de-privacidade.index') }}">Política de Privacidade</a>
    </li>
    <li class="dropdown @if(str_is('painel.meta-technology*', Route::currentRouteName()) || str_is('painel.configuracoes-meta-technology*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Meta Technology
            @if($contatosNaoLidosTech >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidosTech }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.meta-technology.index', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.meta-technology.index') }}">Meta Technology</a>
            </li>
            <li @if(str_is('painel.meta-technology.servicos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.meta-technology.servicos.index') }}">Serviços</a>
            </li>
            <li class="divider"></li>
            <li @if(str_is('painel.meta-technology.contatos-recebidos*', Route::currentRouteName())) class="active" @endif><a href="{{ route('painel.meta-technology.contatos-recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidosTech >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidosTech }}</span>
                @endif
            </a></li>
            <li class="divider"></li>
            <li @if(str_is('painel.configuracoes-meta-technology*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.configuracoes-meta-technology.index') }}">Configurações</a>
            </li>
        </ul>
    </li>
</ul>
