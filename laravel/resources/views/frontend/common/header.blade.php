    <header>
        <div class="center">
            <nav>
                <a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>{{ trans('frontend.nav.grupo') }}</a>
                <a href="{{ route('cases') }}" @if(Tools::isActive('cases')) class="active" @endif>{{ trans('frontend.nav.cases') }}</a>
                <a href="{{ route('depoimentos') }}" @if(Tools::isActive('depoimentos')) class="active" @endif>{{ trans('frontend.nav.depoimentos') }}</a>
                <a href="{{ route('home') }}#contato" @if(Tools::isActive('home')) class="home" @endif>{{ trans('frontend.nav.contato') }}</a>
            </nav>

            <div class="right">
                <div class="links">
                    <a href="http://gibi.folhafacil.com.br/rh/metasp/" class="colaboradores" target="_blank">{{ trans('frontend.nav.colaboradores') }}</a>
                    <a href="http://metarh.agendaunico.com.br/login" class="colaboradores restrita" target="_blank">{{ trans('frontend.nav.area-restrita') }}</a>
                </div>

                <div class="social">
                    <a href="https://www.facebook.com/grupometarh/" target="_blank" class="facebook"></a>
                    <a href="https://www.linkedin.com/company/3190833/" target="_blank" class="linkedin"></a>
                    <a href="https://www.youtube.com/user/GrupoMetaRH" target="_blank" class="youtube"></a>
                </div>

                <?php $lang = app()->getLocale() == 'pt' ? 'en' : 'pt'; ?>
                <a href="{{ route('lang', $lang) }}" class="lang lang-{{ $lang }}"></a>
            </div>
        </div>
    </header>
