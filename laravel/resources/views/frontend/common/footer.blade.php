    <footer>
        <div class="center">
            <div class="col links">
                <a href="{{ route('home') }}">{{ trans('frontend.nav.grupo') }}</a>
                <a href="{{ route('home') }}" class="sub">{{ trans('frontend.nav.quem-somos') }}</a>
                <a href="{{ route('home') }}" class="sub">{{ trans('frontend.nav.executivos') }}</a>
                <a href="{{ route('home') }}" class="sub">{{ trans('frontend.nav.empresas-grupo') }}</a>
                <a href="{{ route('home') }}" class="sub">{{ trans('frontend.nav.missao-visao-valores') }}</a>
                <a href="{{ route('home') }}" class="sub">{{ trans('frontend.nav.responsabilidade') }}</a>
            </div>
            <div class="col links">
                <a href="{{ route('home') }}">{{ trans('frontend.nav.clientes') }}</a>
                <a href="{{ route('cases') }}">{{ trans('frontend.nav.cases') }}</a>
                <a href="{{ route('cases', 'meta-bpo') }}" class="sub">Meta BPO</a>
                <a href="{{ route('cases', 'meta-executivos') }}" class="sub">Meta Executivos</a>
                <a href="{{ route('cases', 'meta-talentos') }}" class="sub">Meta Talentos</a>
                <a href="{{ route('cases', 'meta-technology') }}" class="sub">Meta Technology</a>
            </div>
            <div class="col links">
                <a href="{{ route('depoimentos') }}">{{ trans('frontend.nav.depoimentos') }}</a>
                <a href="{{ route('depoimentos', 'empresas') }}" class="sub">{{ trans('frontend.nav.empresas') }}</a>
                <a href="{{ route('depoimentos', 'profissionais') }}" class="sub">{{ trans('frontend.nav.profissionais') }}</a>
                <a href="{{ route('home') }}">{{ trans('frontend.nav.contato') }}</a>
                <a href="{{ route('home') }}" class="sub">{{ trans('frontend.nav.candidatos') }}</a>
                <a href="{{ route('home') }}" class="sub">{{ trans('frontend.nav.para-empresas') }}</a>
            </div>
            <div class="col dados">
                <p class="telefone">
                    <?php
                        $telefone = explode(' ', $contato->telefone);
                        $numero   = array_splice($telefone, -2);
                    ?>
                    <span>{{ join(' ', $telefone) }}</span>
                    {{ join(' ', $numero) }}
                </p>
                <p class="endereco">{!! $contato->endereco !!}</p>
                <a href="{{ route('politica') }}">{{ trans('frontend.nav.politica') }}</a>
            </div>
            <div class="col copyright">
                <p>© {{ date('Y') }} {{ $config->nome_do_site }} &middot; {{ trans('frontend.copyright.direitos') }}</p>
                <p>
                    <a href="http://www.trupe.net" target="_blank">{{ trans('frontend.copyright.criacao') }}</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
