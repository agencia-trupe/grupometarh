@extends('frontend.common.template')

@section('content')

    <div class="cases">
        <div class="navegacao">
            <h1>{{ trans('frontend.nossos-cases') }}</h1>
            <nav>
                <div class="center">
                @foreach(['meta-bpo', 'meta-executivos', 'meta-talentos', 'meta-technology'] as $d)
                <a href="{{ route('cases', $d) }}" class="{{ $d }} @if($d == $divisao) active @endif"></a>
                @endforeach
                </div>
            </nav>
        </div>

        <div class="cases-main center">
            <div class="cases-lista">
                @foreach($cases as $case)
                <a href="#case-{{ $case->id }}" data-id="{{ $case-> id }}" @if($cases->first() == $case) class="active" @endif>{{ $case->nome }}</a>
                @endforeach
            </div>
            <div class="cases-conteudo">
                @foreach($cases as $case)
                <div class="case" id="case-{{ $case->id }}" @if($cases->first() == $case) style="display:block;" @else style="display:none;" @endif>
                    <div class="titulo">
                        <img src="{{ asset('assets/img/cases/'.$case->marca) }}" alt="">
                        <h2>{{ $case->{'titulo_'.app()->getLocale()} }}</h2>
                    </div>
                    <div class="texto">
                        {!! $case->{'texto_'.app()->getLocale()} !!}
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
