<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">
    <meta name="description" content="{{ $configMT->description }}">
    <meta name="keywords" content="{{ $configMT->keywords }}">

    <meta property="og:title" content="{{ $configMT->title }}">
    <meta property="og:description" content="{{ $configMT->description }}">
    <meta property="og:site_name" content="{{ $configMT->title }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
@if($configMT->imagem_de_compartilhamento)
    <meta property="og:image" content="{{ asset('assets/img/'.$configMT->imagem_de_compartilhamento) }}">
@endif

    <title>{{ $configMT->title }}</title>

    {!! Tools::loadCss('css/vendor.main.css') !!}
    {!! Tools::loadCss('css/main.css') !!}
</head>
<body>
    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="voltar-grupo">
                {{ trans('frontend.meta-technology.voltar') }}
            </a>
            <div class="right">
                <?php $lang = app()->getLocale() == 'pt' ? 'en' : 'pt'; ?>
                <a href="{{ route('lang', $lang) }}" class="lang lang-{{ $lang }}"></a>
            </div>
        </div>
    </header>
    @yield('content')
    @include('frontend.common.footer')

    {!! Tools::loadJquery() !!}
    {!! Tools::loadJs('js/vendor.main.js') !!}
    {!! Tools::loadJs('vendor/slick-carousel/slick/slick.min.js') !!}
    {!! Tools::loadJs('vendor/fancybox/source/jquery.fancybox.pack.js') !!}
    {!! Tools::loadJs('js/main.js') !!}

@if($config->analytics)
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', '{{ $config->analytics }}', 'auto');
        ga('send', 'pageview');
    </script>
@endif
</body>
</html>
