@extends('frontend.meta-technology.template')

@section('content')

    <div class="meta-technology">
        <div class="abertura">
            <div class="divisao"></div>
            <div class="center">
                <div class="texto">
                    <img src="{{ asset('assets/img/layout/mt-marca.png') }}" alt="">
                    {!! $textos->{'abertura_'.app()->getLocale()} !!}
                </div>
            </div>
        </div>

        <div class="frase">
            <div class="center">
                <div class="wrapper">
                    <p>{!! $textos->{'frase_'.app()->getLocale()} !!}</p>
                    <div class="video-wrapper">
                        <div class="video">
                            <iframe src="{{ $textos->{'video_'.app()->getLocale()} }}" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="chamadas">
            <div class="center">
                <div class="texto">
                    @foreach(range(1, 3) as $i)
                    <div class="chamada">
                        <img src="{{ asset('assets/img/layout/mt-icone-'.$i.'.png') }}" alt="">
                        <div>
                            <h3>{{ $textos->{'chamada_'.$i.'_titulo_'.app()->getLocale()} }}</h3>
                            <p>{!! $textos->{'chamada_'.$i.'_texto_'.app()->getLocale()} !!}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="servicos">
            <div class="center">
                <h2>{{ trans('frontend.meta-technology.servicos') }}</h2>
                <div class="servicos-lista">
                    @foreach($servicos as $servico)
                    <div class="servico">
                        <h3>{{ $servico->{'titulo_'.app()->getLocale()} }}</h3>
                        {!! $servico->{'descricao_'.app()->getLocale()} !!}
                        <img src="{{ asset('assets/img/servicos/'.$servico->imagem) }}" alt="">
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="contato">
            <div class="center">
                <h2>{{ trans('frontend.nav.contato') }}</h2>
                <div class="dados">
                    <p class="telefone">{{ $contato->telefone }}</p>
                    <p class="endereco">{!! $contato->endereco !!}</p>
                </div>
                <form action="" id="form-contato-technology" method="POST">
                    <div class="col">
                        <input type="text" name="nome" id="nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
                        <input type="email" name="email" id="email" placeholder="e-mail" required>
                        <input type="text" name="telefone" id="telefone" placeholder="{{ trans('frontend.contato.telefone') }}">
                    </div>
                    <textarea name="mensagem" id="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required></textarea>
                    <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
                    <div id="form-contato-response"></div>
                </form>
            </div>
            <div class="mapa">
                {!! $contato->google_maps !!}
            </div>
        </div>

        <div class="news">
            @include('frontend.meta-technology.metanews')
        </div>
    </div>

@endsection
