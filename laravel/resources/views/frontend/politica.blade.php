@extends('frontend.common.template')

@section('content')

    <div class="politica">
        <div class="center">
            <h1>{{ trans('frontend.nav.politica') }}</h1>
            <div class="texto">{!! $politica->{'texto_'.app()->getLocale()} !!}</div>
        </div>
    </div>

@endsection
