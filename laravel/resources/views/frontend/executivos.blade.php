<div class="executivos-lightbox">
    <div class="title">{{ trans('frontend.nav.executivos') }}</div>
    <div class="thumbs">
        @foreach($executivos as $executivo)
        <a href="#" class="executivo">
            <img src="{{ asset('assets/img/executivos/'.$executivo->imagem) }}" alt="">
            <div class="overlay">
                <h3>
                    {{ $executivo->nome }}
                    <span>{{ $executivo->{'cargo_'.app()->getLocale()} }}</span>
                </h3>
            </div>
            <div class="texto" style="display:none">
                <h3>
                    {{ $executivo->nome }}
                    <span>{{ $executivo->{'cargo_'.app()->getLocale()} }}</span>
                </h3>
                <p>{!! $executivo->{'texto_'.app()->getLocale()} !!}</p>
            </div>
        </a>
        @endforeach
    </div>
    <div class="interna" style="display:none">
        <div class="imagem"></div>
        <div class="texto"></div>
        <a href="#" class="executivos-voltar">&laquo; {{ app()->getLocale() == 'pt' ? 'voltar' : 'back' }}</a>
    </div>
</div>
