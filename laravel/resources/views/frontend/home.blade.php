@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            <div class="banners-cycle">
                @foreach($banners as $banner)
                <div class="slide" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})" data-frase="{{ $banner->{'frase_'.app()->getLocale()} }}"></div>
                @endforeach
            </div>

            <div class="faixas">
                <div class="faixa-grupo">
                    <div class="center">
                        <img src="{{ asset('assets/img/layout/grupo-meta-rh-marca.png') }}" alt="">
                        @if(count($banners))
                        <p>{{ $banners->first()->{'frase_'.app()->getLocale()} }}</p>
                        @endif
                    </div>
                </div>

                <div class="faixa-divisoes desktop">
                    <span>{{ trans('frontend.home.conheca') }}</span>
                    <div class="divisao meta-bpo">
                        <img src="{{ asset('assets/img/layout/meta-bpo-marca.png') }}" alt="">
                        <div class="links">
                            <a href="{{ $divisoes->meta_bpo_site }}" target="_blank">{{ trans('frontend.home.acessar-site') }} &raquo;</a>
                            <a href="{{ $divisoes->meta_bpo_vagas }}" target="_blank">{{ trans('frontend.home.ver-vagas') }} &raquo;</a>
                        </div>
                    </div>
                     <div class="divisao meta-executivos">
                        <img src="{{ asset('assets/img/layout/meta-executivos-marca.png') }}" alt="">
                        <div class="links">
                            <a href="{{ $divisoes->meta_executivos_site }}" target="_blank">{{ trans('frontend.home.acessar-site') }} &raquo;</a>
                            <a href="{{ $divisoes->meta_executivos_vagas }}" target="_blank">{{ trans('frontend.home.ver-vagas') }} &raquo;</a>
                        </div>
                    </div>
                     <div class="divisao meta-talentos">
                        <img src="{{ asset('assets/img/layout/meta-talentos-marca.png') }}" alt="">
                        <div class="links">
                            <a href="{{ $divisoes->meta_talentos_site }}" target="_blank">{{ trans('frontend.home.acessar-site') }} &raquo;</a>
                            <a href="{{ $divisoes->meta_talentos_vagas }}" target="_blank">{{ trans('frontend.home.ver-vagas') }} &raquo;</a>
                        </div>
                    </div>
                     <div class="divisao meta-technology">
                        <img src="{{ asset('assets/img/layout/meta-technology-marca.png') }}" alt="">
                        <div class="links">
                            <a href="{{ $divisoes->meta_technology_site }}" target="_blank">{{ trans('frontend.home.acessar-site') }} &raquo;</a>
                            <a href="{{ $divisoes->meta_technology_vagas }}" target="_blank">{{ trans('frontend.home.ver-vagas') }} &raquo;</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="faixa-divisoes mobile">
            <div class="center">
                <span>{{ trans('frontend.home.conheca') }}</span>
                <div class="divisao meta-bpo">
                    <img src="{{ asset('assets/img/layout/meta-bpo-marca.png') }}" alt="">
                    <div class="links">
                        <a href="{{ $divisoes->meta_bpo_site }}">{{ trans('frontend.home.acessar-site') }} &raquo;</a>
                        <a href="{{ $divisoes->meta_bpo_vagas }}">{{ trans('frontend.home.ver-vagas') }} &raquo;</a>
                    </div>
                </div>
                 <div class="divisao meta-executivos">
                    <img src="{{ asset('assets/img/layout/meta-executivos-marca.png') }}" alt="">
                    <div class="links">
                        <a href="{{ $divisoes->meta_executivos_site }}">{{ trans('frontend.home.acessar-site') }} &raquo;</a>
                        <a href="{{ $divisoes->meta_executivos_vagas }}">{{ trans('frontend.home.ver-vagas') }} &raquo;</a>
                    </div>
                </div>
                 <div class="divisao meta-talentos">
                    <img src="{{ asset('assets/img/layout/meta-talentos-marca.png') }}" alt="">
                    <div class="links">
                        <a href="{{ $divisoes->meta_talentos_site }}">{{ trans('frontend.home.acessar-site') }} &raquo;</a>
                        <a href="{{ $divisoes->meta_talentos_vagas }}">{{ trans('frontend.home.ver-vagas') }} &raquo;</a>
                    </div>
                </div>
                 <div class="divisao meta-technology">
                    <img src="{{ asset('assets/img/layout/meta-technology-marca.png') }}" alt="">
                    <div class="links">
                        <a href="{{ $divisoes->meta_technology_site }}">{{ trans('frontend.home.acessar-site') }} &raquo;</a>
                        <a href="{{ $divisoes->meta_technology_vagas }}">{{ trans('frontend.home.ver-vagas') }} &raquo;</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="linha-do-tempo">
            <div class="center">
                <h2>GRUPO META RH <span>| {{ trans('frontend.home.quem-somos') }}</span></h2>
                <div class="linha-wrapper">
                    <a href="#" class="seta-voltar">&laquo;</a>
                    <a href="#" class="seta">&raquo;</a>
                    @foreach($linhaDoTempo as $data)
                        @foreach($data as $registro)
                        <div class="registro">
                            @if($data->first() == $registro)
                            <span>{{ $registro->{'data_'.app()->getLocale()} }}</span>
                            @endif
                            <div class="marcador"></div>
                            <p>{!! $registro->{'texto_'.app()->getLocale()}  !!}</p>
                        </div>
                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>

        <div class="missao-visao-valores">
            <div class="center">
                <div class="col">
                    <h3>{{ trans('frontend.home.missao') }}</h3>
                    {!! $textos->{'missao_'.app()->getLocale()} !!}
                </div>
                <div class="col">
                    <h3>{{ trans('frontend.home.visao') }}</h3>
                    {!! $textos->{'visao_'.app()->getLocale()} !!}
                </div>
                <div class="col">
                    <h3>{{ trans('frontend.home.valores') }}</h3>
                    {!! $textos->{'valores_'.app()->getLocale()} !!}
                </div>
            </div>
        </div>

        <div class="video-diferenciais">
            <div class="center">
                <div class="left">
                    <div class="video">
                        <iframe src="{{ $video->{'video_'.app()->getLocale()} }}" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="selos">
                        @foreach($selos as $selo)
                        <div class="selo">
                            <img src="{{ asset('assets/img/selos/'.$selo->imagem) }}" alt="">
                            @if($selo->{'texto_'.app()->getLocale()})
                            <div class="popup">{{ $selo->{'texto_'.app()->getLocale()} }}</div>
                            @endif
                        </div>
                        @endforeach
                        <div class="selo" style="max-width:114px">
                            <script language="JavaScript" src="http://dunsregistered.dnb.com" type="text/javascript"></script>
                        </div>
                    </div>
                </div>
                <div class="right">
                    <h3>{{ trans('frontend.home.diferenciais') }}</h3>
                    {!! $textos->{'diferenciais_competitivos_'.app()->getLocale()} !!}
                </div>
            </div>
        </div>

        <div class="responsabilidade">
            <div class="center">
                <h3>{{ trans('frontend.home.executivos') }}</h3>
                <div class="texto">
                    <p>{{ trans('frontend.home.executivos-texto') }}</p>
                </div>
                <div class="link-executivos-wrapper">
                    <a href="{{ route('executivos') }}" class="lightbox-open">{{ trans('frontend.home.conheca-mais') }} &raquo;</a>
                </div>
            </div>
        </div>

        <div class="responsabilidade">
            <div class="center">
                <h3>{{ trans('frontend.home.responsabilidade') }}</h3>
                <div class="texto">
                    {!! $textos->{'responsabilidade_social_'.app()->getLocale()} !!}
                </div>
                <div class="empresas">
                    @foreach($empresas as $empresa)
                    <div class="imagem">
                        <img src="{{ asset('assets/img/responsabilidade-social/'.$empresa->marca) }}" alt="">
                        <div class="overlay">
                            <h4>{{ $empresa->nome }}</h4>
                            <p>{{ $empresa->{'texto_'.app()->getLocale()} }}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="clientes">
            <div class="center">
                <h3>{{ trans('frontend.home.clientes') }}</h3>
                <div class="box">
                    <div class="clientes-slides">
                        <a href="#" class="clientes-seta">&raquo;</a>
                        <a href="#" class="clientes-seta-voltar">&laquo;</a>
                    @foreach($clientes as $cliente)
                        <img data-lazy="{{ asset('assets/img/clientes/'.$cliente->marca) }}" alt="">
                    @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="news">
            @include('frontend.metanews')
        </div>

        <div class="contato" id="contato">
            <div class="center">
                <h3>{{ trans('frontend.nav.contato') }}</h3>
                <div class="dados">
                    <p class="telefone">{{ $contato->telefone }}</p>
                    <p class="endereco">{!! $contato->endereco !!}</p>
                </div>
                <div class="links">
                    <a href="{{ route('contato.candidatos') }}" class="lightbox-open">{{ trans('frontend.nav.candidatos') }}</a>
                    <a href="{{ route('contato.empresas') }}" class="lightbox-open">{{ trans('frontend.nav.para-empresas') }}</a>
                </div>
            </div>
            <div class="mapa">
                {!! $contato->google_maps !!}
            </div>
        </div>
    </div>

@endsection
