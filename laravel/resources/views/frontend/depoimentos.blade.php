@extends('frontend.common.template')

@section('content')

    <div class="depoimentos">
        <div class="navegacao">
            <div class="center">
                <h1>{{ trans('frontend.nav.depoimentos') }}</h1>
            </div>
            <nav>
                <div class="center">
                @foreach(['empresas', 'profissionais'] as $d)
                <a href="{{ route('depoimentos', $d) }}" @if($d == $tipo) class="active" @endif>
                    {{ trans('frontend.nav.'.$d) }}
                </a>
                @endforeach
                </div>
            </nav>
        </div>

        @if($tipo == 'profissionais')
        <div class="depoimentos-profissionais">
            <div class="center">
                @foreach($depoimentos as $depoimento)
                <div class="depoimento">
                    {!! $depoimento->{'texto_'.app()->getLocale()} !!}
                    <p class="autor">{{ $depoimento->{'autor_'.app()->getLocale()} }}</p>
                </div>
                @endforeach
            </div>
        </div>
        @else
        <div class="depoimentos-empresas">
            <div class="center">
                <a href="#" class="depoimentos-anterior" data-cur="0" data-max="{{ count($depoimentos) - 1 }}">&raquo;</a>
                <a href="#" class="depoimentos-proximo" data-cur="0" data-max="{{ count($depoimentos) - 1 }}">&raquo;</a>

                @foreach($depoimentos as $key => $depoimento)
                <div class="depoimento" id="depoimento-{{ $key }}" @if($depoimentos->first() == $depoimento) style="display:block;" @else style="display:none;" @endif>
                    <div class="titulo">
                        <div class="imagem">
                            <img src="{{ asset('assets/img/depoimentos-empresas/'.$depoimento->marca) }}" alt="">
                        </div>
                        <h3>{{ $depoimento->{'titulo_'.app()->getLocale()} }}</h3>
                    </div>
                    <div class="texto">
                        {!! $depoimento->{'texto_'.app()->getLocale()} !!}
                        <p class="autor">{!! $depoimento->{'autor_'.app()->getLocale()} !!}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif
    </div>

@endsection
