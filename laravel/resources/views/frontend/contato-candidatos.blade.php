<div class="contato-candidatos">
    <h2>{{ trans('frontend.nav.candidatos') }}</h2>

    <div class="wrapper">
        <h3>{{ trans('frontend.contato.titulo-candidatos') }}</h3>

        <div class="divisoes">
            <a target="_blank" href="{{ $divisoes->meta_bpo_vagas }}">
                <img src="{{ asset('assets/img/layout/meta-bpo-marca-cor.png') }}" alt="">
                <span>{{ app()->getLocale() == 'pt' ? 'clique aqui' : 'click here' }}</span>
            </a>
            <a target="_blank" href="{{ $divisoes->meta_executivos_vagas }}">
                <img src="{{ asset('assets/img/layout/meta-executivos-marca-cor.png') }}" alt="">
                <span>{{ app()->getLocale() == 'pt' ? 'clique aqui' : 'click here' }}</span>
            </a>
            <a target="_blank" href="{{ $divisoes->meta_talentos_vagas }}">
                <img src="{{ asset('assets/img/layout/meta-talentos-marca-cor.png') }}" alt="">
                <span>{{ app()->getLocale() == 'pt' ? 'clique aqui' : 'click here' }}</span>
            </a>
            <a target="_blank" href="{{ $divisoes->meta_technology_vagas }}">
                <img src="{{ asset('assets/img/layout/meta-technology-marca-cor.png') }}" alt="">
                <span>{{ app()->getLocale() == 'pt' ? 'clique aqui' : 'click here' }}</span>
            </a>
        </div>

        <form action="" id="form-contato-candidatos" method="POST">
            <span>{{ trans('frontend.contato.duvidas') }}</span>
            <div class="col">
                <input type="text" name="nome" id="nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="{{ trans('frontend.contato.telefone') }}">
            </div>
            <textarea name="mensagem" id="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required></textarea>
            <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
            <div id="form-contato-response"></div>
        </form>
    </div>
</div>
