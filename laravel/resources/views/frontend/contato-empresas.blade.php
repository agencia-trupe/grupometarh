<div class="contato-empresas">
    <h2>{{ trans('frontend.nav.para-empresas') }}</h2>

    <div class="wrapper">
        <form action="" id="form-contato-empresas" method="POST">
            <div class="left">
                <div class="box">
                    <p>
                        {{ trans('frontend.contato.contrate') }}
                        <span>{{ trans('frontend.contato.recrute') }}</span>
                    </p>
                </div>
                <p>{!! trans('frontend.contato.politica') !!}</p>
            </div>
            <div class="col">
                <input type="text" name="nome" id="nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
                <input type="text" name="empresa" id="empresa" placeholder="{{ trans('frontend.contato.empresa') }}" required>
                <input type="text" name="telefone" id="telefone" placeholder="{{ trans('frontend.contato.telefone') }}" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <div class="contratacao">
                    <p>{{ trans('frontend.contato.contratacao') }}</p>
                    <label>
                        <input type="checkbox" name="contratacao[]" value="Efetivo" required>
                        {{ trans('frontend.contato.efetivo') }}
                    </label>
                    <label>
                        <input type="checkbox" name="contratacao[]" value="Temporário" required>
                        {{ trans('frontend.contato.temporario') }}
                    </label>
                    <label>
                        <input type="checkbox" name="contratacao[]" value="Terceiro" required>
                        {{ trans('frontend.contato.terceiro') }}
                    </label>
                    <label>
                        <input type="checkbox" name="contratacao[]" value="Estágio" required>
                        {{ trans('frontend.contato.estagio') }}
                    </label>
                    <label>
                        <input type="checkbox" name="contratacao[]" value="Trainee" required>
                        {{ trans('frontend.contato.trainee') }}
                    </label>
                </div>
            </div>
            <div class="col">
                <select name="regiao" id="regiao" required>
                    <option value="" selected>{{ trans('frontend.contato.regiao') }}</option>
                    <option value="Norte">{{ trans('frontend.contato.norte') }}</option>
                    <option value="Nordeste">{{ trans('frontend.contato.nordeste') }}</option>
                    <option value="Sudeste">{{ trans('frontend.contato.sudeste') }}</option>
                    <option value="Sul">{{ trans('frontend.contato.sul') }}</option>
                    <option value="Centro-Oeste">{{ trans('frontend.contato.centro-oeste') }}</option>
                </select>
                <select name="como_conheceu" id="como_conheceu" required>
                    <option value="" selected>{{ trans('frontend.contato.como_conheceu') }}</option>
                    <option value="Google">Google</option>
                    <option value="Indicações">{{ trans('frontend.contato.indicacoes') }}</option>
                    <option value="E-mail">E-mail</option>
                    <option value="Redes Sociais">{{ trans('frontend.contato.redes-sociais') }}</option>
                    <option value="Outros">{{ trans('frontend.contato.outros') }}</option>
                </select>
                <textarea name="descricao" id="descricao" placeholder="{{ trans('frontend.contato.descricao') }}"></textarea>
            </div>
            <div class="envio">
                <label class="arquivo-input">
                    <input type="file" name="arquivo" id="arquivo">
                    <span>{{ trans('frontend.contato.anexar') }}</span>
                </label>
                <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
            </div>
            <div id="form-contato-response"></div>
        </form>
    </div>
</div>
