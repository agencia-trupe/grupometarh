export default function Banners() {
    $('.banners-cycle').cycle({
        slides:'>.slide',
        timeout: 8000
    }).on('cycle-before', function(event, opts, curr, next) {
        $('.faixa-grupo p').fadeOut(function() {
            $(this).text($(next).data('frase'));
            $(this).fadeIn();
        });
    });
};
