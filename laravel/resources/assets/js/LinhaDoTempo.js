export default function LinhaDoTempo() {
    $('.linha-wrapper').slick({
        draggable: false,
        slide: '.registro',
        nextArrow: '.seta',
        prevArrow: '.seta-voltar',
        infinite: false,
        slidesToShow: 5,
        responsive: [
            {
                breakpoint: 768,
                settings: { slidesToShow: 1 }
            },
            {
                breakpoint: 1280,
                settings: { slidesToShow: 3 }
            },
        ]
    });
};
