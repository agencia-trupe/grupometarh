export default function Depoimentos() {
    $('.depoimentos-proximo').click(function(event) {
        event.preventDefault();

        var max  = $(this).data('max'),
            cur  = $(this).data('cur'),
            next = ++cur;

        if (next > max) {
            next = 0;
        }

        $(this).data('cur', next);
        $('.depoimentos-anterior').data('cur', next);
        $('.depoimento').css('display', 'none');
        $('#depoimento-' + next).css('display', 'block');
    });

    $('.depoimentos-anterior').click(function(event) {
        event.preventDefault();

        var max  = $(this).data('max'),
            cur  = $(this).data('cur'),
            next = --cur;

        if (next < 0) {
            next = $(this).data('max');
        }

        $(this).data('cur', next);
        $('.depoimentos-proximo').data('cur', next);
        $('.depoimento').css('display', 'none');
        $('#depoimento-' + next).css('display', 'block');
    });
};
