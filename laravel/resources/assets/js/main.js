import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import Cases from './Cases';
import Depoimentos from './Depoimentos';
import Banners from './Banners';
import LinhaDoTempo from './LinhaDoTempo';

AjaxSetup();
MobileToggle();
Cases();
Depoimentos();
Banners();
LinhaDoTempo();

$('.lightbox-open').fancybox({
    helpers: {
        overlay: {
            locked: false,
            css: {'background-color': 'rgba(0,0,0,.8)'},
        }
    },
    type: 'ajax',
    maxWidth: 1200,
    padding: 0,
    closeBtn: false
});

$(document).on('click', '.executivo', function(event) {
    event.preventDefault();

    var bg  = $(this).css('background-color'),
        img = $(this).find('img').clone(),
        txt = $(this).find('.texto').html();

    $('.interna .imagem').html(img).css('background-color', bg);
    $('.interna .texto').html(txt);

    $('.executivos-lightbox .thumbs').hide();
    $('.executivos-lightbox .interna').show();

    $.fancybox.update();
});

$(document).on('click', '.executivos-voltar', function(event) {
    event.preventDefault();

    $('.executivos-lightbox .interna').hide();
    $('.executivos-lightbox .thumbs').show();

    $.fancybox.update();
});

$(document).on('submit', '#form-contato-candidatos', function(event) {
    event.preventDefault();

    var $form     = $(this),
        $response = $('#form-contato-response');

    if ($form.hasClass('sending')) return false;

    $response.fadeOut('fast');
    $form.addClass('sending');

    $.ajax({
        type: "POST",
        url: $('base').attr('href') + '/contato-candidatos',
        data: {
            nome: $('#nome').val(),
            email: $('#email').val(),
            telefone: $('#telefone').val(),
            mensagem: $('#mensagem').val(),
        },
        success: function(data) {
            $response.fadeOut().text(data.message).fadeIn('slow');
            $form[0].reset();
        },
        error: function(data) {
            if (data.responseJSON) {
                var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                $response.fadeOut().text(error).fadeIn('slow');
            }
        },
        dataType: 'json'
    })
    .always(function() {
        $form.removeClass('sending');
    });
});

$(document).on('submit', '#form-contato-technology', function(event) {
    event.preventDefault();

    var $form     = $(this),
        $response = $('#form-contato-response');

    if ($form.hasClass('sending')) return false;

    $response.fadeOut('fast');
    $form.addClass('sending');

    $.ajax({
        type: "POST",
        url: $('base').attr('href') + '/meta-technology/contato',
        data: {
            nome: $('#nome').val(),
            email: $('#email').val(),
            telefone: $('#telefone').val(),
            mensagem: $('#mensagem').val(),
        },
        success: function(data) {
            $response.fadeOut().text(data.message).fadeIn('slow');
            $form[0].reset();
        },
        error: function(data) {
            if (data.responseJSON) {
                var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                $response.fadeOut().text(error).fadeIn('slow');
            }
        },
        dataType: 'json'
    })
    .always(function() {
        $form.removeClass('sending');
    });
});

var defaultTextFile;

$(document).on('change', '.arquivo-input input', function(event) {
    if (!defaultTextFile) {
        defaultTextFile = $('.arquivo-input span').text();
    }

    var files = event.target.files;

    if (!files) return;

    if (files.length) {
        var fileName = event.target.files[0].name;
        $(this).next().text(fileName).parent().addClass('active');
    } else {
        var fileName = defaultTextFile;
        $(this).next().text(fileName).parent().removeClass('active');
    }
});

$(document).on('change', 'input:checkbox[name="contratacao[]"]', function(e) {
    console.log('clicked');
    if ($('input:checkbox[name="contratacao[]"]:checked').length > 0) {
        $('input:checkbox[name="contratacao[]"]').attr('required', false);
    } else {
        $('input:checkbox[name="contratacao[]"]').attr('required', true);
    }
});

$(document).on('submit', '#form-contato-empresas', function(event) {
    event.preventDefault();

    var $form     = $(this),
        $response = $('#form-contato-response');

    if ($form.hasClass('sending')) return false;

    $response.fadeOut('fast');
    $form.addClass('sending');

    $.ajax({
        type: "POST",
        url: $('base').attr('href') + '/contato-empresas',
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function(data) {
            $response.fadeOut().text(data.message).fadeIn('slow');
            if (defaultTextFile) {
                $('.arquivo-input span').html(defaultTextFile).parent().removeClass('active');
            }
            $form[0].reset();
        },
        error: function(data) {
            if (data.responseJSON) {
                var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                $response.fadeOut().text(error).fadeIn('slow');
            }
        },
        dataType: 'json'
    })
    .always(function() {
        $form.removeClass('sending');
    });
});

$('.clientes-slides').slick({
    draggable: false,
    slide: 'img',
    nextArrow: '.clientes-seta',
    prevArrow: '.clientes-seta-voltar',
    slidesToShow: 5,
    slidesToScroll: 5,
    autoplay: true,
    lazyLoad: 'progressive',
});
