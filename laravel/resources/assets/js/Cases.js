export default function Cases() {
    $('.cases-lista a').click(function(event) {
        event.preventDefault();

        if ($(this).hasClass('active')) return;

        $('.cases-lista a').removeClass('active');
        $(this).addClass('active');

        $('.case').css('display', 'none');
        $('#case-' + $(this).data('id')).css('display', 'block');
    });
};
